package io.ds.webcrawler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
/*
    Just the implementation of the WebGrab class from the slides.
    Start via <Run>-Link, not 'gradle run'. Main class of gradle project is WebCrawler.
*/
public class WebGrab {
    
    public static void main(String[] args) throws Exception {
        if (args.length!=1) {
            System.out.println("No url specified");
            System.exit(1);
        }
        URL myUrl = new URL("args[0].trim()");
        HttpURLConnection con = (HttpURLConnection)myUrl.openConnection();
        BufferedReader read = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String line = read.readLine();
        while(line!=null) {
            System.out.println(line);
            line = read.readLine();
        }
    }
}

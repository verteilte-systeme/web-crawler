package io.ds.webcrawler;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class WebCrawler {    
    /*
     * args[0]: initial URL for web crawler
     */
    public static void main(String[] args) {
        if (args.length == 1) {
            String initialURL = args[0];
            Set<String> emails = new HashSet<>();//Use a set to avoid duplicates!

            //Start crawler thread
            Thread search = new CrawlerThread(emails, initialURL);
            search.setDaemon(true);//crawler thread terminates when main thread terminates!
            search.start();

            //Wait for user input
            System.out.println("Press <Enter> to Stop Crawler!");
            Scanner console = new Scanner(System.in);//Wait until user presses Enter-key
            console.nextLine();

            //Print results
            System.out.println("Final set of emails: " + emails);
            console.close();
        } else {
            System.out.println("No initial URL defined!");
        }
    }
}

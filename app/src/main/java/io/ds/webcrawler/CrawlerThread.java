package io.ds.webcrawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CrawlerThread extends Thread {

    private Set<String> emails;
    private Queue<String> links = new LinkedList<>();
    private List<String> visited = new LinkedList<>();
    private String initialURL;

    public CrawlerThread(Set<String> emails, String initialURL) {
        this.emails = emails;
        this.initialURL = initialURL;
        links.add(initialURL);
    }

    @Override
    public void run() {
        while (!links.isEmpty()) {
            // Read web site
            String nextURL = links.remove().trim();
            visited.add(nextURL);
            try {
                System.out.println("Start reading from URL: " + nextURL);
                URL myUrl = new URL(nextURL);
                HttpURLConnection con = (HttpURLConnection) myUrl.openConnection();
                BufferedReader read = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuilder webSite = new StringBuilder();
                String line = read.readLine();
                while (line != null) {
                    webSite.append(line);
                    line = read.readLine();
                }

                // search for email addresses
                Pattern email = Pattern.compile("[a-zA-Z\\.]{2,30}@[a-zA-Z\\.-]{2,20}\\.[a-z]{2,3}");
                Matcher matcher = email.matcher(webSite.toString());
                while (matcher.find()) {
                    String match = matcher.group();                    
                    emails.add(match);
                }

                // search for links
                Pattern link = Pattern.compile("href=\"([a-zA-Z\\./-:0-9]{5,25})\"");
                matcher = link.matcher(webSite.toString());
                while (matcher.find()) {
                    String match = matcher.group(1);                    
                    String newURL = null;
                    if (match.startsWith("/")) {
                        if (initialURL.endsWith("/")) {
                            newURL = initialURL + match.substring(1);                            
                        } else {
                            newURL = initialURL + match;
                        }
                    } else if (match.startsWith("http")) {
                        newURL=match;
                    }
                    if (!visited.contains(newURL)&&!links.contains(newURL)&&newURL!=null)
                        links.add(newURL);
                }
            } catch (IOException e) {
                System.err.println("WebCrawler: Can't read URL: " + nextURL);
            }
        }
        System.out.println("WebCrawler: No more links to scan!");
        System.out.println("Press <Enter> to show result list.");
    }
}
